#!/usr/bin/python
# -*- coding: utf-8 -*-
# read a level and print things and/or listen for user input and print things then
# send all triggered cues to main computer
# open a server for incomming cue triggers from main computer
#
#

#from escpos import *
import driver
import json
import sys
import re
import threading
import signal
import network

from time import sleep

level = []
filename = ""
tcp_connection = None
cue = {}
running = False
hardware = False

listeners = []

# Function: signal_handler
# global function to register a quit operation
def signal_handler(signal, frame):
    quit()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

def quit():
    global running
    running = False
    tcp_connection.quit()
    listener_thread.join()

#
# evaluate cue Dict
#
def dispatch(name):
    for c in level:
        if c["name"] == name:
            global cue
            cue = c
            
            send = {}
            send["cue"] = name
            tcp_connection.send_json(send)
            
            if "print" in cue:
                for p in cue["print"]:
                    pinprint(p)
                    
            if not "input" in cue:
                if "else" in cue:
                    dispatch(cue["else"])
                else:
                    print("There is neither an input listener nor an 'else' defined in %s" % name)
            return
    print("No cue with name: %s" % name)

# checks on user input and compares it with current cue
def listen():
    while running:
        user_in = input("\n> ")
        
        if not running:
            break
            
        pinprint(">>> " + user_in)

        if not check_input(user_in):
            if "else" in cue:
                dispatch(cue["else"])
            elif "name" in cue:
                print("There was no match in %s and no followcue defined" % cue["name"])
            else:
                print("No cue dispatched yet!")

def check_input(user_in):
    if "input" in cue:
        for listener in cue["input"]:
            if listener["if"][0] == '/':
                reg_if = listener["if"][1:].rsplit('/',1)[0]
                #print("REGEX: %s" % reg_if)
                if re.search(reg_if, user_in, re.IGNORECASE) is not None:
                    dispatch(listener["then"])
                    return True
            elif user_in.lower() == listener["if"].lower():
                dispatch(listener["then"])
                return True
    return False
            
# Print the text to console and printer
def pinprint(text):
    text = text.replace("\\n","\n")
    if hardware:
        printer.write(text + "\n\n\n\n\n")
    else:
        print(text)
    
def incomming(data):
    print("incomming:%s" % data)
    if "cue" in data:
        dispatch(data['cue'])
    if "load" in data:
        get_level(data["load"])
    if "reload" in data:
        get_level(filename)
    
def get_level(file):
    with open(file, 'r', encoding='utf-8') as level_file:
        try:
            level_file = json.load(level_file)
            global filename
            filename = file
            global level
            level = level_file["cues"]
            print("Loaded Level %s" % level_file["name"])
        except ValueError as e:
            print("Level JSON has errors: %s" % e)
            #quit()
            sys.exit(0)
            
        
if __name__ == "__main__":
    
    argv = sys.argv[1:]
    
    preset = ""
    level = {}
    
    server_port = 8080
    remote_port = 8090
    remote_host = "localhost"
    
    if len(argv) > 0:
        get_level(argv[0])            
        running = True
        
        if len(argv) > 3:
            server_port = int(argv[1])
            remote_host = argv[2]
            remote_port = int(argv[3])
            
        tcp_connection = network.TCP_connection(remote_host, remote_port)
        tcp_connection.listen(server_port, incomming)
        
        listener_thread = threading.Thread(name = 'listener', target = listen, args = ())
        listener_thread.start()
        
        try:
            printer = driver.Pinwriter ('/dev/usb/lp0')
            hardware = True
        except FileNotFoundError as e:
            print("%s\nRunning without printing device"%e)
        
        dispatch("INIT")
    else:
        print("Please provide a filename")
