#!/usr/bin/python
# -*- coding: utf-8 -*-
#
class Pinwriter:
	def __init__(self, port):
		print("opening pinwriter on port " + port)
		self.device = open(port,'wb+',buffering=0)
		self.device.write(b"\x1b@\x1b@\x1bR\x02\x1bH\x1bF")

	def write(self, text):
		print("print:", text)
		text = text.replace("Ä","[")
		text = text.replace("Ö","\\")
		text = text.replace("Ü","]")
		text = text.replace("ä","{")
		text = text.replace("ö","|")
		text = text.replace("ü","}")
		self.device.write(bytearray(text,'iso-8859-1'));





if __name__ == "__main__":
	p = Pinwriter ('/dev/usb/lp0')
	text = "Test täxt mit sönnenschün"
	p.write(text + "\n")
	#p.write(u"Test täxt mit sönnenschün\n")
