# pinwriter

A json formatted text adventure for the command line that also works with a keyboard and an Epson pinwriter.

## Setup

Access Raspberry Pi via ssh with

`ssh pi@masterHandler`

Adjust IP Address of remote server

`nano Documents/Disaster/pinwriter/launch.sh`

Change the IP in the following line:

`screen -S pinwriter -m bash -c 'cd /home/pi/Documents/Disaster/pinwriter; sudo python3 pinwriter.py Bombenservice4.json 9080 "192.168.0.2" 9070'`

## Troubleshooting

After accessing pi via ssh, log into pinwriter *screen* process with

`screen -rx`

To detach from *screen* and go back to pi user use *CTRL + A* and *CTRL + D*