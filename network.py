#!/usr/bin/python
# -*- coding: utf-8 -*-
import socket
import threading
import json

# Class: TCP_connection
# Allows to send and receive UDP messages
# makes use of socket module: https://wiki.python.org/moin/TcpCommunication
class TCP_connection(object):
	def __init__(self, host, port, name="tcp_connection"):
		self.host = host
		self.port = port
		self.buffersize = 1024
		self.name = name

		self.sending = None

		self.status = {}
		self.reset_status()

	def reset_status(self):
		self.status["success"] = 0
		self.status["failed"] = []

	# Function report
	# collects and prints feedback on recent tcp send
	# returns dict formattet feedback
	def report(self):
		feedback = {}
		feedback["failed"] = []
		feedback["success"] = "TCP send successfull for %s clients" % self.status["success"]
		print(feedback["success"])

		if self.status["failed"]:
			#feedback["failed"].append("TCP send failed for: ")
			#print(feedback["failed"][0])
			for fails in self.status["failed"]:
				print("- %s" % fails)
				feedback["failed"].append("- %s" % fails)

		self.status["success"] = 0
		self.status["failed"] = []

		return feedback

	# Function: send
	# send json fromatted TCP
	# open thread for parallel response checking
	def send(self, message, host=None, port=None, timeout=5.0):
		if host is None:
			host = self.host
		if port is None:
			port = self.port

		self.sending = threading.Thread(name = "send_process", target = self.send_process, args = (message, host, port, timeout))
		self.sending.start()

	# Function: send_json
	# send json fromatted TCP
	# open thread for parallel response checking
	def send_json(self, message, host=None, port=None, timeout=5.0):
		if host is None:
			host = self.host
		if port is None:
			port = self.port

		self.sending = threading.Thread(name = "send_process", target = self.send_process, args = (json.dumps(message), host, port, timeout))
		self.sending.start()

	# Function: send_process
	# create connection and wait for response
	def send_process(self, message, host, port, timeout):
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.settimeout(timeout)
			s.connect((host, port))
			s.send(message.encode())
			data = s.recv(self.buffersize)
			s.close()
			if data == "success" or data == message:
				self.status["success"] = self.status["success"] +1;
			else:
				self.status["failed"].append("%s on Port %d : %s\n" % (host, port, data))
			#print "TCP %s from %s port %s\n" % (data, host, port)

		except socket.error as e:
			#print("TCP sending to %s on Port %d failed: %s\n" % (host, port, e) )
			self.status["failed"].append("%s on Port %d : %s\n" % (host, port, e))
	
	def listen(self, server_port, callback):
		self.rcv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.rcv.bind(("0.0.0.0", server_port))
		self.rcv.listen(1)
		self.running = True
		
		print("%s awaiting TCP messages on port %s" % (__name__, server_port))
		self.tcpserver = threading.Thread(name = "tcpserver", target = self.receive_json, args = (self.rcv, callback))
		self.tcpserver.start()

	def receive_json(self, rcv, callback):
		while self.running:
			conn, addr = rcv.accept()
			
			data = conn.recv(self.buffersize)
			if not data:break
			try:
				jsondata = json.loads(data.decode('utf-8'))

				callback(jsondata)
				print("incomming TCP msg: %s from %s\n" % ( str(data), str(addr) ) )
			except ValueError as e:
				print("Error receiving TCP json msg: %s" %e)
			conn.close()
		print("TCP Server Thread ended")
				
		

	def quit(self):
		self.running = False

		if self.sending is not None:
			if self.sending.isAlive():
				self.sending.join(2.0)
				if self.sending.isAlive():
					print('could not join thread: %s' % self.sending.name)

# Class: UDP_connection
# Allows to send and receive UDP messages
# makes use of socket module: https://wiki.python.org/moin/UdpCommunication
class UDP_connection(object):
	def __init__(self, host, port, name="udp_connection"):
		self.rcv = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.rcv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

		self.host = host
		self.port = port
		self.server_port = None
		self.name = name

		#if server_port is not None:
		#	rcv.bind(('', server_port)) # wenn udpport = 0 wird zufaelliger Port vergeben
		#	server_port = rcv.getsockname()[1]
		#
		#	self.running = True
		#	self.udpserver = threading.Thread(name = "udpserver", target = self.receive_json, args = (server_port,))
		#	self.udpserver.start()

		

	def send_json(self, message, host=None, port=None):
		#print("Message %s" % message)
		s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

		if host is None:
			host = self.host
		if port is None:
			port = self.port

		s.connect((host, port))
		s.settimeout(2)
		s.send(json.dumps(message))
		s.close()

		#print("send:%s TO %s:%d" % (message, host, port) )

	def await_cue(self, server_port, cuebucket):
		self.rcv.bind(('', server_port)) # wenn udpport = 0 wird zufaelliger Port vergeben
		self.server_port = self.rcv.getsockname()[1]

		self.running = True
		self.udpserver = threading.Thread(name = "udpserver", target = self.receive_json, args = (server_port, cuebucket))
		self.udpserver.start()

	def receive_json(self, port, cuebucket):
		print("%s awaiting UDP messages on port %s" % (__name__, port))

		while self.running:
			
			data, addr = self.rcv.recvfrom(1024)
			print("incomming udp msg: %s from %s\n" % ( str(data), str(addr) ) )
			try:
				jsondata = json.loads(data)

				if "Name" in jsondata:
					# add Port to cue to signify where it comes from
					# Obsolete By 03.November 2017
					jsondata["Host"] = self.host
					jsondata["Port"] = self.port
					cuebucket.append(jsondata)
			except ValueError as e:
				print("Error receiving UDP json msg: %s" %e)

			

	# Function: quit
	# close udp connection
	def quit(self):
		#print 'close network'
		#self.udpserver.join()

		# Den udpserver killen:
		if self.server_port is not None:
			try:
				#self.rcv.shutdown(socket.SHUT_RDWR)
				self.rcv.shutdown(socket.SHUT_RD)
				self.rcv.close()
			except socket.error as e:
				print('problem shutting down udp socket on port %s: %s ' % (self.server_port, e))

			self.running = False

			if self.udpserver.isAlive():
				self.udpserver.join(2.0)
				if self.udpserver.isAlive():
					print('could not join thread: %s' % self.udpserver.name)




if __name__ == '__main__':
	udp_test = UDP_connection("localhost", 7001, 7000)

	while True:
		newline = raw_input("\n")

		in_list = newline.split(" ")

		if len(in_list) == 2:
			# dispatch a cue
			if in_list[0] == "send":
				udp_test.send_json(in_list[1])
			else:
				print('no valid input "%s %s". try again:' % (in_list[0], in_list[1]))
		else:
			print('no valid input %s. try again:' % in_list)
